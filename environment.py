from math import sin, cos, pi, sqrt, acos

import numpy as np

class MotorTask:

    def motorDimension(self):
        raise NotImplementedError

    def taskDimension(self):
        raise NotImplementedError

    def predict(self, motorCommand):
        raise NotImplementedError


class GroundTouchingArm(MotorTask):

    def __init__(self, motorDimension=3, shoulderHeight=0.1, armLength=1.0, minGroundDistanceForTouch=0.05):
        if motorDimension < 3:
            raise ValueError("Arm dimension needs to be at lest M=3")
        self.M = motorDimension
        self.shoulderHeight = shoulderHeight
        self.armLength = armLength
        self._segmentLength = armLength / (self.M-1.0)
        self._minDistance = minGroundDistanceForTouch

    def motorDimension(self):
        return self.M

    def taskDimension(self):
        return 2

    def predict3D(self, motorCommand):
        if np.size(motorCommand)!=(self.motorDimension()):
            raise ValueError("ThreeLinkTouch:predict(): unexpected input size")

        lateralX = 0.0
        lateralY = self.shoulderHeight
        totalAngle = 0.0
        for jointIdx in range(1,self.M):
            totalAngle += motorCommand[jointIdx]
            lateralX += cos(totalAngle) * self._segmentLength
            lateralY += sin(totalAngle) * self._segmentLength

        planeAngle = motorCommand[0]
        x = cos(planeAngle) * lateralX
        y = sin(planeAngle) * lateralX
        z = lateralY

        return np.array([x, y, z])

    def predict(self, motorCommand):

        position = self.predict3D(motorCommand)

        # ground touched?
        if abs(position[2]) < self._minDistance:
            return np.array([position[0],position[1]])
        else:
            return None

    def linkPositions3D(self, motorCommand):
        if np.size(motorCommand) != (self.motorDimension()):
            raise ValueError("ThreeLinkTouch:predict(): unexpected input size")

        pos = np.zeros((self.motorDimension()+1,3))

        pos[0,:] = np.array([0,0,0])
        pos[1,:] = np.array([0,0,self.shoulderHeight])

        planeAngle = motorCommand[0]
        lateralX = 0.0
        lateralY = self.shoulderHeight
        totalAngle = 0.0
        for jointIdx in range(1, self.M):
            totalAngle += motorCommand[jointIdx]
            lateralX += cos(totalAngle) * self._segmentLength
            lateralY += sin(totalAngle) * self._segmentLength

            x = cos(planeAngle) * lateralX
            y = sin(planeAngle) * lateralX
            z = lateralY
            pos[jointIdx+1] = np.array([x,y,z])

        return pos

    def suggestHomePosture(self):
        M = self.motorDimension()
        homePendingAngle = - pi / (M)
        # set shoulder angle to touch ground
        homePosture = np.ones((M)) * homePendingAngle
        homePosture[0] = 0.0
        homePosture[1] = pi / 2
        pos3d = self.predict3D(homePosture)
        # print("Home posture:" + str(homePosture))
        # print("Home position:" + str(pos3d))
        height = 0.1
        diffFromShoulder = pos3d - np.array([0, 0, height])
        distance = np.linalg.norm(diffFromShoulder)
        pointOnGroundFromShoulder = np.array([sqrt(distance ** 2 - height ** 2), 0, -height])
        correctionAngle = acos(np.dot(diffFromShoulder / distance, pointOnGroundFromShoulder / distance))
        print(correctionAngle)
        homePosture[1] -= correctionAngle
        pos3d = self.predict3D(homePosture)
        # print("After correction:")
        # print("Home posture:" + str(homePosture))
        # print("Home position:" + str(pos3d))

        return homePosture

if __name__ == '__main__':
    fwd = GroundTouchingArm()
    print(fwd.predict(np.array([0,0,0])))
    print(fwd.predict(np.array([0,-0.1,0])))
    print(fwd.predict(np.array([0,-0.2,0.1])))
    print(fwd.predict(np.array([0.5,-0.2,0.1])))
    print(fwd.predict(np.array([pi/2,-0.2,0.1])))
    print(fwd.predict(np.array([pi/2,-0.2,1])))