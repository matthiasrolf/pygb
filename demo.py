from math import pi, sqrt, acos, asin, ceil

from environment import GroundTouchingArm
from gb import GoalBabbling
from models import LinearModel, LocalLinearModel
import numpy as np
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D


# =================================================================
# Setup task / environment
# =================================================================

M = 5
env = GroundTouchingArm(motorDimension=M, armLength=0.6)
N = env.taskDimension()


# =================================================================
# Setup home posture (make sure it creates a measurement, no 'None' result)
# =================================================================

## A nice choice (in contact with surface, generates an outcome)
homePosture = env.suggestHomePosture()

# ## Far from surface (no outcome generated)
# homePosture = env.suggestHomePosture()
# homePosture[1] += 1

# ## Stretched arm. Close to table but no contact. Kinematically arkward (singularity).
# homePosture = np.zeros(M)

homePosition = env.predict(homePosture)
homePosition3D = env.predict3D(homePosture)

startGoal = None
if homePosition is None:
    startGoal = np.array([0.0,0.3])

print("Home posture:"+str(homePosture))
print("Home position (3D):"+str(homePosition3D))
print("Home position:"+str(homePosition))
print("Start goal:"+str(startGoal))

# =================================================================
# Specify targets
# =================================================================
targetList = []
for x in np.linspace(0.0,0.6, 5):
    for y in np.linspace(-0.6, 0.6, 9):
        r = np.sqrt(x * x + y * y)
        if 0.2 <= r <= 0.5:
            targetList.append(np.array([x,y]))


# =================================================================
# Setup inverse model
# =================================================================
model = LocalLinearModel(N, M, dist=0.1)

# =================================================================
# Search for good initialization if home posture did not create an outcome
# =================================================================
if homePosition is None:
    closestPosture = None
    closestOutcome = None
    closestDistance = float("Inf")
    t = 0
    while t < 100 or closestOutcome is None:
        randomMotorCommand = homePosture + np.random.randn(M)*0.3
        outcome = env.predict(randomMotorCommand)
        if outcome is not None:
            d = np.linalg.norm(randomMotorCommand-homePosture)
            if d < closestDistance:
                closestPosture = randomMotorCommand
                closestOutcome = outcome
                closestDistance = d
        t += 1
    print("Init posture:"+str(closestPosture))
    print("Init position:"+str(closestOutcome))
    model.adapt(closestOutcome,closestPosture,1.0)
    model.offsets[0] = closestPosture

# =================================================================
# Setup exploration
# =================================================================
goHomeProbability=0.1
goalStepLength=0.05
motorStepLength=0.1
gb = GoalBabbling(env, model, homePosture, targetList, startGoal=startGoal,
                  goHomeProbability=goHomeProbability, goalStepLength=goalStepLength,
                  motorStepLength=motorStepLength)
gb.setMotorPerturbation(0.02)

# =================================================================
# Run exploration and learning
# =================================================================
T = 10000 # Total exploration steps

XS = np.zeros((T, N)) # Explored targets
X = np.zeros((T, N)) # Outcomes (including 'None's)
X3D = np.zeros((T, 3)) # Full 3D outcomes (no 'None's)
Q = np.zeros((T, M)) # Motor commands
for t in range(T):
    # Exploration
    sample = gb.generateSample()

    # Learning
    if sample.outcome is not None:
        model.adapt(sample.outcome, sample.motor, 1 * sample.weight)

    # Logging and storing data.....
    if sample.outcome is not None:
        X[t, :] = sample.outcome
    else:
        X[t, :] = np.nan
    if sample.target is not None:
        XS[t, :] = sample.target
    else:
        XS[t, :] = np.nan
    Q[t, :] = sample.motor
    X3D[t, :] = env.predict3D(sample.motor)

    # print("x*: " + str(sample.target))
    # print("x:  " + str(sample.outcome))
    # print("q:  " + str(sample.motor))
    # print("w:  " + str(sample.weight))
    # print()

# =================================================================
# Evaluation
# =================================================================
print("Individual samples generated: " + str(T))
print("Number of point-to-point movements: " + str(gb.numCompletedMovements))
print("Total motor turns: " + str(gb.totalMotorDistance/(2*pi)))
numTests = len(targetList)
numTouched = 0
numReached = 0
avgDistance2D = 0.0
for target in targetList:
    q = model.predict(target)
    x = env.predict(q)
    if x is not None:
        numTouched += 1
        distance = np.linalg.norm(target-x)
        if distance <= 0.05:
            numReached += 1
            avgDistance2D += distance
if numReached is not 0:
    avgDistance2D /= numReached
print("Number of targets generating touches: " + str(numTouched) + "/" + str(numTests))
print("Number of targets reached: " + str(numReached) + "/" + str(numTests))
print("Average reaching error: " + str(avgDistance2D))

# =================================================================
# Plot 2D results and generated touches
# =================================================================
pyplot.plot(X[:, 0], X[:, 1], label="Positions", color=(0.1, 0.5, 0.7))
pyplot.plot(XS[:, 0], XS[:, 1], label="Targets", color=(1.0, 0.5, 0.05))
pyplot.xlabel("surface x")
pyplot.ylabel("surface y")
pyplot.legend()
pyplot.show()

# =================================================================
# Plot full 3D motion
# =================================================================
fig = pyplot.figure()
ax = fig.gca(projection='3d')
ax.plot(XS[:, 0], XS[:, 1], np.zeros((T)), label='Targets', color=(1.0, 0.5, 0.05))
ax.plot(X3D[:, 0], X3D[:, 1], X3D[:, 2], label='Positions', color=(0.1, 0.5, 0.7), alpha=0.5)
numExampleArms = 20
for exampleIdx in range(numExampleArms):
    t = ceil(T/numExampleArms * exampleIdx)
    positions = env.linkPositions3D(Q[t,:])
    ax.plot(positions[:,0], positions[:,1], positions[:,2],
            label=("Arm" if exampleIdx==0 else ""), color="red", alpha=0.5)

ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")
ax.legend()
pyplot.show()