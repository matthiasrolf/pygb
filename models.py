

import numpy as np

class LinearModel:
    def __init__(self, inputDim, outputDim):
        self.inputDim = inputDim
        self.outputDim = outputDim
        self.mat = np.zeros((outputDim, inputDim ), dtype=float)
        self.off = np.zeros((outputDim), dtype=float)

    def predict(self, input):
        if np.size(input)!=self.inputDim:
            raise ValueError("LinearModel:predict(): unexpected input size")
        return np.dot(self.mat, input) + self.off

    def adapt(self, input, outputTarget, rate):
        if np.size(input)!=self.inputDim:
            raise ValueError("LinearModel:adapt(): unexpected input size")
        if np.size(outputTarget)!=self.outputDim:
            raise ValueError("LinearModel:adapt(): unexpected target size")
        o = np.dot(self.mat, input) + self.off
        e = outputTarget - o

        dMat = np.outer((rate*e), input)
        self.mat += dMat
        self.off += rate*e

class PiecewiseLinearModel:
    def __init__(self, inputDim, outputDim, firstCenter=None, dist=0.2):
        self.inputDim = inputDim
        self.outputDim = outputDim
        self.maxDist = dist
        if firstCenter is not None:
            self.centers = [firstCenter]
        else:
            self.centers = [np.zeros((inputDim))]
        self.models = [LinearModel(inputDim,outputDim)]

    def sqNorm(self, x):
        s = 0.0
        for i in range(x.size):
            s += x[i] * x[i]
        return s

    def findClosest(self, input):
        minDist = self.sqNorm(input - self.centers[0])
        minIdx = 0
        for i in range(1, len(self.centers)):
            d = self.sqNorm(input - self.centers[i])
            if d < minDist:
                minDist = d
                minIdx = i
        return minDist, minIdx

    def predict(self, input):
        if np.size(input)!=self.inputDim:
            raise ValueError("LinearModel:predict(): unexpected input size")
        distance, idx = self.findClosest(input)
        wInput = (input - self.centers[idx]) * (1.0 / self.maxDist)
        return self.models[idx].predict(wInput)

    def adapt(self, input, outputTarget, rate):
        if np.size(input)!=self.K:
            raise ValueError("LinearModel:adapt(): unexpected input size")
        if np.size(outputTarget)!=self.K:
            raise ValueError("LinearModel:adapt(): unexpected target size")
        distance, idx = self.findClosest(input)
        if np.sqrt(distance) > self.maxDist:
            self.centers.append(input)
            self.models.append(LinearModel(self.inputDim, self.outputDim))
            idx = len(self.models)-1
        wInput = (input - self.centers[idx]) * (1.0 / self.maxDist)
        self.models[idx].adapt(wInput, outputTarget, rate)


class LocalLinearModel:
    def __init__(self, inputDim, outputDim, firstCenter=None, dist=0.2):
        self.inputDim = inputDim
        self.outputDim = outputDim
        self.maxDist = dist
        if firstCenter is not None:
            self.centers = [firstCenter]
        else:
            self.centers = [np.zeros((inputDim))]
        self.matrices = [np.zeros((outputDim,inputDim), dtype=float)]
        self.offsets = [np.zeros((outputDim), dtype=float)]

    def getSquareDistancesFromDifference(self, diff):
        # minDist = float("inf")
        minDist = 1e1000
        distances = np.zeros(len(self.centers))
        for i in range(len(self.centers)):
            d = 0.0
            for k in range(self.inputDim):
                d += diff[i][k]*diff[i][k]
            distances[i] = d
            if d < minDist:
                minDist = d
        return distances, minDist

    def getNormDifferences(self, w):
        differences = []
        for i in range(len(self.centers)):
            d = (w - self.centers[i])*(1/self.maxDist)
            differences.append(d)
        return differences

    def getLocalModelWeightsFromNormDistances(self, distances, minDist):
        weights = np.exp((distances-minDist)*(-0.5)*2)
        weights *= (1.0/sum(weights))
        return weights

    def getLocalModelWeights(self, w):
        diff = self.getNormDifferences(w)
        dist, minDist = self.getSquareDistancesFromDifference(diff)
        weights = self.getLocalModelWeightsFromNormDistances(dist,minDist)
        return weights

    def predict(self, input):
        if np.size(input)!=self.inputDim:
            raise ValueError("LinearModel:predict(): unexpected input size")

        diff = self.getNormDifferences(input)
        dist, minDist = self.getSquareDistancesFromDifference(diff)
        weights = self.getLocalModelWeightsFromNormDistances(dist,minDist)

        output = np.zeros(self.outputDim)
        for idx in range(len(self.centers)):
            if 0.001 < weights[idx]:
                output += weights[idx] * (np.dot(self.matrices[idx],diff[idx])+self.offsets[idx])

        return output

    def adapt(self, input, outputTarget, rate):
        if np.size(input)!=self.inputDim:
            raise ValueError("LinearModel:adapt(): unexpected input size")
        if np.size(outputTarget)!=self.outputDim:
            raise ValueError("LinearModel:adapt(): unexpected target size")

        diff = self.getNormDifferences(input)
        dist, minDist = self.getSquareDistancesFromDifference(diff)
        weights = self.getLocalModelWeightsFromNormDistances(dist,minDist)

        if minDist > 1.0:
            # compute value and slope for initialization of new model
            mat = np.zeros((self.outputDim,self.inputDim), dtype=float)
            off = np.zeros((self.outputDim), dtype=float)
            for idx in range(len(self.centers)):
                if 0.001 < weights[idx]:
                    mat += weights[idx] * self.matrices[idx]
                    off += weights[idx] * (np.dot(self.matrices[idx],diff[idx])+self.offsets[idx])

            self.centers.append(input)
            self.matrices.append(mat)
            self.offsets.append(off)

            # todo incremental update
            diff = self.getNormDifferences(input)
            dist, minDist = self.getSquareDistancesFromDifference(diff)
            weights = self.getLocalModelWeightsFromNormDistances(dist, minDist)

        for idx in range(len(self.centers)):
            if 0.001 < weights[idx]:
                o = np.dot(self.matrices[idx], diff[idx] ) + self.offsets[idx]

                e = outputTarget - o
                e = (weights[idx]*rate) * e

                dMat = np.outer(e, diff[idx])
                self.matrices[idx] += dMat
                self.offsets[idx] += e

