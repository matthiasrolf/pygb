from math import isnan, sqrt
from random import random, randint

import numpy as np


class Sample:
    def __init__(self, outcome=None, outcomePrev=None, motor=None, motorPrev=None, target=None, targetPrev=None,
                 weight=None):
        self.outcome = outcome
        self.outcomePrev = outcomePrev
        self.motor = motor
        self.motorPrev = motorPrev
        self.target = target
        self.targetPrev = targetPrev
        self.weight = weight


class GoalBabbling:
    class State:
        def __init__(self, parentGB):
            self.gb = parentGB

        def next(self):
            raise NotImplementedError

    class StateGenerateHomePosture(State):
        def __init__(self, parentGB):
            super().__init__(parentGB)

        def next(self):
            homePosture = self.gb.homePosture
            outcome = self.gb.task.predict(homePosture)
            # startGoal = outcome
            if outcome is not None:
                startGoal = outcome
            else:
                if self.gb.startGoal is not None:
                    startGoal = self.gb.startGoal
                else:
                    raise RuntimeError("Could not establish starting position")
            return Sample(outcome=outcome, motor=homePosture, weight=1.0), \
                   GoalBabbling.StateGoalPath(self.gb, previousGoal=startGoal, previousOutcome=outcome)

    class StateGoalPath(State):
        def __init__(self, parentGB, previousGoal, previousOutcome, endGoalIndex=None):
            super().__init__(parentGB)

            self.previousGoal = previousGoal
            self.previousOutcome = previousOutcome

            if endGoalIndex is None:
                self.endGoalIndex = randint(0, len(parentGB.targetList) - 1)
            else:
                self.endGoalIndex = endGoalIndex
            self.endGoal = parentGB.targetList[self.endGoalIndex]
            self.anyOutcomeMeasured = False

        def next(self):
            goalDirection = self.endGoal - self.previousGoal
            distance = np.linalg.norm(goalDirection)
            maxStepLength = self.gb.goalStepLength
            # note: some tolerance build to avoid numeric problems from tiniest residual steps in the next
            # exploration step
            if maxStepLength*1.1 < distance:
                goalStep = (maxStepLength / distance) * goalDirection
                goalStepLength = (maxStepLength / distance)
                endGoalReached = False
            else:
                goalStep = goalDirection
                goalStepLength = distance
                endGoalReached = True
            goal = self.previousGoal + goalStep
            motorCommand = self.gb.model.predict(goal) + self.gb.stepMotorPerturbation()
            outcome = self.gb.task.predict(motorCommand)

            previousMotorCommand = self.gb.getLastMotorCommand()

            weight = 0.0
            if outcome is not None and self.previousOutcome is not None:

                outcomeStep = outcome - self.previousOutcome
                outcomeStepLength = np.linalg.norm(outcomeStep)
                movementCosine = np.dot(goalStep, outcomeStep) / (goalStepLength * outcomeStepLength)
                wDir = 0.5 * (1 + movementCosine)

                motorStepLength = np.linalg.norm(motorCommand - previousMotorCommand)
                wEff = outcomeStepLength / motorStepLength

                weight = wDir * wEff

                if isnan(weight):
                    weight = 0.0

            if weight > 0.0:
                self.anyOutcomeMeasured = True

            sample = Sample(outcome=outcome, outcomePrev=self.previousOutcome, motor=motorCommand,
                            motorPrev=previousMotorCommand, target=goal, targetPrev=self.previousGoal, weight=weight)

            self.previousOutcome = outcome
            self.previousGoal = goal

            if endGoalReached:
                self.gb.numCompletedMovements += 1
                if not self.anyOutcomeMeasured or random() < self.gb.goHomeProbability:
                    return sample, GoalBabbling.StateGoHomePosture(self.gb, previousOutcome=outcome)
                else:
                    nextEndGoalIndex = self.endGoalIndex
                    while nextEndGoalIndex == self.endGoalIndex:
                        nextEndGoalIndex = randint(0, len(self.gb.targetList) - 1)
                    return sample, GoalBabbling.StateGoalPath(
                        self.gb, previousGoal=goal, previousOutcome=outcome, endGoalIndex=nextEndGoalIndex)
            else:
                return sample, self

    class StateGoHomePosture(State):
        def __init__(self, parentGB, previousOutcome):
            super().__init__(parentGB)
            self.previousOutcome = previousOutcome

        def next(self):
            homePosture = self.gb.homePosture
            previousMotorCommand = self.gb.getLastMotorCommand()
            homeDirection = homePosture - previousMotorCommand
            distance = np.linalg.norm(homeDirection)
            maxStepLength = self.gb.motorStepLength
            if maxStepLength < distance:
                homeStep = (maxStepLength / distance) * homeDirection
                homeReached = False
            else:
                homeStep = homeDirection
                homeReached = True
            motorCommand = previousMotorCommand + homeStep
            motorCommand = motorCommand + self.gb.stepMotorPerturbation()
            if 0 < self.gb.homeDrag:
                motorCommand += (self.gb.homePosture - motorCommand)*self.gb.homeDrag
            outcome = self.gb.task.predict(motorCommand)

            weight = 0.0
            if outcome is not None and self.previousOutcome is not None:
                outcomeStep = outcome - self.previousOutcome
                outcomeStepLength = np.linalg.norm(outcomeStep)
                motorStepLength = np.linalg.norm(motorCommand - previousMotorCommand)
                wEff = outcomeStepLength / motorStepLength
                weight = wEff
                if isnan(weight):
                    weight = 0.0

            sample = Sample(outcome=outcome, outcomePrev=self.previousOutcome, motor=motorCommand,
                            motorPrev=previousMotorCommand, weight=weight)
            self.previousOutcome = outcome

            if homeReached:
                # start off from unperturbed home position to make sure goal direction is well defined
                homePosition = self.gb.task.predict(homePosture)
                self.gb.numCompletedMovements += 1
                # return sample, GoalBabbling.StateGoalPath(self.gb, previousGoal=homePosition, previousOutcome=outcome)
                if homePosition is not None:
                    startGoal = homePosition
                else:
                    if self.gb.startGoal is not None:
                        startGoal = self.gb.startGoal
                    else:
                        raise RuntimeError("Could not establish starting position")
                return sample, GoalBabbling.StateGoalPath(self.gb, previousGoal=startGoal, previousOutcome=outcome)
            else:
                return sample, self

    def __init__(self, task, model, homePosture, targetList, startGoal=None,
                 goHomeProbability=0.1, goalStepLength=0.05, motorStepLength=0.1):
        self.task = task
        self.model = model
        self.homePosture = homePosture
        self.startGoal = startGoal
        self.targetList = targetList

        self.state = GoalBabbling.StateGenerateHomePosture(self)

        self.lastSample = None

        self.goHomeProbability = goHomeProbability
        self.homeDrag = 0.0

        self.goalStepLength = goalStepLength
        self.motorStepLength = motorStepLength

        self._motorPerturbationSize = 0.0
        self._motorPerturbationUpdate = 0.0
        self._motorPerturbationRescaling = 0.0
        self._motorPerturbation = None
        self.setMotorPerturbation(0.02)

        self.totalMotorDistance = 0.0
        self.numCompletedMovements = 0

    def generateSample(self):
        sample, self.state = self.state.next()

        if self.lastSample is not None:
            motorDistance = np.linalg.norm(sample.motor - self.lastSample.motor)
            self.totalMotorDistance += motorDistance

        self.lastSample = sample
        return sample

    def getLastMotorCommand(self):
        if self.lastSample is None:
            return None
        else:
            return self.lastSample.motor

    def setMotorPerturbation(self, size, updateSize=None):
        self._motorPerturbationSize = size
        if updateSize is None:
            self._motorPerturbationUpdate = size/5
        else:
            self._motorPerturbationUpdate = updateSize

        s0 = self._motorPerturbationSize ** 2
        s1 = self._motorPerturbationUpdate ** 2
        self._motorPerturbationRescaling = sqrt(s0 / (s0+s1))
        self._motorPerturbation = np.random.normal(scale=self._motorPerturbationSize,
                                                   size=(self.task.motorDimension()))

    def setHomeDrag(self, size):
        self.homeDrag = size

    def stepMotorPerturbation(self):
        # Auto-regressive Process or kind AR(1) / discrete Ornstein–Uhlenbeck process
        # Overall mean: 0.0
        # Overall variance: self.motorPerturbationSize**2
        # Update variance per step: self.motorPerturbationUpdate**2
        self._motorPerturbation += np.random.normal(scale=self._motorPerturbationUpdate,
                                                    size=(self.task.motorDimension()))
        self._motorPerturbation *= self._motorPerturbationRescaling
        return self._motorPerturbation

